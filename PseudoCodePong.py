


Set Up Game Board (maybe use a "board" class)
    - make boarders  (boarders class or objects class)
    - make paddles   (paddle class or objects class)
    - make ball      (probably a separate class)
    - put ball and paddles in initial position

        
Game
    - start ball after short delay
    - check ball position against objects to determine if they have collided
        - if ball hits object/barrier, change angles/velocity so that the ball 
              "bounces" off and continues in another direction
        - keep ball moving until it hits an object/barrier
            - use velocity to advance ball one position at a time
    - check ball position to see if goal has been scored (i.e. if ball is past the paddles)
        - if true, add score to appropriate player's score
        - reset the board
            - ball and paddles back to original position
    - check to see if keyboard buttons have been pressed
        - if correct buttons pressed, move paddle in correct direction
        - BUT limit the paddle range of motion to be only within the top and bottom boarders
        
GUI
    - add game board to GUI
    - make restart button
        - reset the board and the score when restart is pushed
    - make a quit button
        - exit game/window when pushed
    - make a label for the score
    - check for key events
         - bind key events to moving the paddles up or down
    - 




Jack's Pddle mover'

class Paddle():
    def __init__(self):
        self.position = 150
        self.velocity = 0
        self.key_delta = 1
        
        
    def up(self): 
    """
    Event method for:
        - up press
        - down release
    """
        self.velocity += 1
        
    def down(self):
    """
    Event method for:
        - down press
        - up release
    """
        self.velocity -= 1
        
    def move(self):
        self.position += self.velocity
        
        
    
class Pong(Frame):
    def __init__(self):
        self.paddle = Paddle()
        self.press_action = {'w': self.paddle.up,
                             's': self.paddle.down }
        self.release_action = {'w': self.paddle.down,
                               's': self.paddle.up     }
        
        self.bind_all('<KeyPress>', self.press)
        self.binda_all('<KeyRelease>', self.release)
    
    def press(self, event):
        self.press_action[event.keysym]()
    
    def release(self, event):
        self.release_action[event.keysym]() 
        
        
        
        
          
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        