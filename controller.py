#Pong Controller

import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt

class Circle:
	def __init__(self, pos, vel):
		self.radius = 5
		self.position = np.array(pos, dtype=float)
		self.velocity = np.array(vel, dtype=float)

	def new_velocity(self, line):

		new_dir = np.dot(self.velocity, line)
		self.velocity = self.velocity - 2*new_dir*line
		

	def new_position(self):
		self.position = self.position+self.velocity


class Line:
	def __init__(self, positions):
		self.positions = positions
		self.p1 = np.array([positions[0], positions[1]])
		self.p2 = np.array([positions[2], positions[3]])

		self.vector = np.array([self.p1[0]-self.p2[0], self.p1[1]-self.p2[1]])
		self.unitv = self.vector/norm(self.vector)
		self.unormal = np.array([-self.unitv[1], self.unitv[0]])

		self.step = 10

	def move_line(self, vert):
		#vert refers to up or down direction represetned by +1 or -1
		self.p1[1] = self.p1[1]+vert*self.step
		self.p2[1] = self.p2[1]+vert*self.step
		self.positions[1] = self.p1[1]
		self.positions[3] = self.p2[1]
		
	def hit_check(self,pos,rad):
		self.hit = False

		v1 = np.array([pos[0]-self.p1[0], pos[1]-self.p1[1]])
		angle = np.arccos(np.dot(v1, self.unitv)/norm(v1))
		d = v1 * np.sin(angle)
		l = v1 * np.cos(angle)

		if norm(d) <= rad and norm(l) < norm(self.vector):
			self.hit = True
		return self.hit
