#Pong Game Paddles Prototype

import sys
import random
from Tkinter import *
import numpy as np

import time
from numpy import sin, cos, pi, array


WIDTH = 600
HEIGHT = 300
DELAY =  100   
 

class Ball():
    
    def __init__(self, radius, speed, angle, position_of_center_x, 
                    position_of_center_y, canvas_object, tag):
        # use velocity [x,y]
        # OR use speed and angle, but don't need both
        
        # for testing this part
        # ------------
        self.direction = [2,2]
        #----------
        
        self.radius = radius
        self.speed = speed           
        self.angle= angle    
        
        self.position_x = position_of_center_x
        self.position_y = position_of_center_y

        self.canvas_object = canvas_object
        self.tag = tag
        
        top_left_x = self.position_x - self.radius
        top_left_y = self.position_y - self.radius
        bottom_right_x = self.position_x + self.radius
        bottom_right_y = self.position_y + self.radius
        
        self.original_position_coords = (top_left_x, top_left_y, bottom_right_x ,
                                          bottom_right_y)
        
        self.ball_canv_obj = self.canvas_object.create_oval(top_left_x, top_left_y,
                            bottom_right_x, bottom_right_y, fill = 'red', tag = self.tag) 
        # or call it self.ball_canv_obj = 
         
#     def update_ball(self, angle):           #   TOLERENCE, SPEED, and SIZE need to be related
# 
#         self.position_x += self.speed*cos(angle)     
#         self.position_y += self.speed*sin(angle) 
#         self.canvas_object.move(self.ball_im, self.speed*cos(angle), 
#                                          self.speed*sin(angle))

# Might add this class for next phase
# class Objects():
# 
#     def __init__(self)



class Board(Canvas):

    def __init__(self, parent, Frame):
        field = Canvas.__init__(self, width=WIDTH, height=HEIGHT,
                                    background="black", highlightthickness=0)
        
        
        self.DELAY = 50
        self.Frame = Frame      # this is the GUI Frame passed back from protopong
        self.parent = parent
        
        self.initGame()
        self.pack()
        

    def initGame(self):

        self.time = 0

        self.paddle_A = 100
        self.paddle_B = 100

        self.balls = []

        self.score_player1 = 0           # added to keep score
        self.score_player2 = 0           # added to keep score

        self.hit_counter = 0
        self.hit_counter2 = 0
        label_p1 = Label(self.Frame, text = "  Player 1                               ")
        label_p1.grid(row = 0, column = 0, columnspan=2)

        label_p2 = Label(self.Frame, text = "                               Player 2  ")
        label_p2.grid(row = 0, column = 8, columnspan=2)
        
        self.score_labels()
        
        self.focus_get()
        self.createObjects()
        self.bind_all("<Key>", self.onKeyPressed)       ### KEY BINDING ###
        self.after(self.DELAY, self.onTimer)


    def createObjects(self):
        self.create_rectangle(550, self.paddle_A, 540, self.paddle_A+50,
                fill="white", tag="paddle_A")
        self.create_rectangle(50, self.paddle_B, 40, self.paddle_B+50, 
                fill="white", tag="paddle_B")
        self.addtag_withtag("paddle", "paddle_A")
        self.addtag_withtag("paddle", "paddle_B")

        self.ball = Ball(5, 2, pi/4, 300, 150, self, "ball")

        self.create_line(0, 0, 600, 0, fill="white", tag="border")
        self.create_line(0, 300, 600, 300, fill="white", tag="border")

         ### CREATE CENTER LINE ###
        self.create_line(300,0,300,300, fill="white", tag = "center_line")


        #self.ball2 = Ball( 7, 2, pi/2, 100, 100, self, "ball")

        self.balls.append(self.ball)
        #self.balls.append(self.ball2)


    def checkCollision(self):

        border = self.find_withtag("border")
        ball = self.find_withtag("ball")
        paddle = self.find_withtag("paddle")

        for ii in range(0, len(ball)):
            x1, y1, x2, y2, = self.bbox(ball[ii])
            overlap = self.find_overlapping(x1, y1, x2, y2)

            for line in border:
                for over in overlap:
                    if over == line:
                        self.balls[ii].direction[1] = -self.balls[ii].direction[1]
                        
            for line in paddle:
                for over in overlap:
                    if over == line:
                        self.balls[ii].direction[0] = -self.balls[ii].direction[0]
                        self.hit_counter+=1
                        self.hit_counter2+=1
                        # could associate the hit counter with a specific
                        #    ball when using multiple balls
            
            if self.hit_counter == 5:
                self.ball.direction[0] *=1.1     # ONLY increasing speed of original ball
                self.ball.direction[1] *=1.1
                self.hit_counter = 0
            if self.hit_counter2 == 12:
                self.add_ball()
                self.hit_counter2 = 0

    
    def add_ball(self):
        new_ball = Ball(14, .5, pi/4, 375, 200, self, "ball")
        self.balls.append(new_ball)
    
    def reset_after_score(self):
        self.ball_im = self.find_withtag("ball") 
        if len(self.balls) > 2:
            for ii in range (2, len(self.balls)):
                self.delete(self.ball_im[ii])
            self.balls = [self.balls[0], self.balls[1]]
        
        
        self.padA = self.find_withtag("paddle_A")    
        self.padB = self.find_withtag("paddle_B")     
        self.ball_im = self.find_withtag("ball") 
        
        self.coords(self.padA, (550, self.paddle_A, 540, self.paddle_A+50))
        self.coords(self.padB, (50, self.paddle_B, 40, self.paddle_B+50))
        for ii in range(0, len(self.ball_im)):
            self.coords(self.ball_im[ii], self.balls[ii].original_position_coords)
        
        self.score_labels()
        self.ball.direction[0] = 2
        self.ball.direction[1] = 2
        self.hit_counter = 0
        self.hit_counter2 = 0


    def score_labels(self):
        label_score = Label(self.Frame, text = "  Score  ")
        label_score.grid(row = 0, column = 3) 
        
        self.label_p1_score_value = Label(self.Frame,text='  %s  '%self.score_player1)
        self.label_p1_score_value.grid(row = 0, column = 2)
        
        self.label_p2_score_value = Label(self.Frame,text='  %s  '%self.score_player2)
        self.label_p2_score_value.grid(row = 0, column = 4)
        

    def doMove(self):

        ball_im = self.find_withtag("ball") 
        for ii in range(0, len(ball_im)):
            if self.coords(ball_im[ii])[0] < 0:
                self.score_player2 +=1
                self.after(200, self.reset_after_score())
                break
            elif self.coords(ball_im[ii])[0] > 600:
                self.score_player1 += 1
                self.after(200, self.reset_after_score())
                break
            else:
                self.move(ball_im[ii], self.balls[ii].direction[0], 
                                self.balls[ii].direction[1])


    def onKeyPressed(self, e):

        key = e.keysym

        padA = self.find_withtag("paddle_A")
        padB = self.find_withtag("paddle_B")
        border = self.find_withtag("border")

        if key == "Up" :
            if self.coords(padA)[1] > 0 :
                self.move(padA, 0, -5)
        if key == "Down":
            if self.coords(padA)[3] <300:
                self.move(padA, 0, 5)
        if key == "e":
            if self.coords(padB)[1] > 0:
                self.move(padB, 0, -5)
        if key == "s":
            if self.coords(padB)[3] < 300:
                self.move(padB, 0, 5)            


    def onTimer(self):

        self.checkCollision()
        self.doMove()
        self.after(self.DELAY, self.onTimer)
        self.time = self.time + 1

    def gameOver(self):
        self.delete(ALL)
        self.create_text(self.winfo_width()/2, self.winfo_height()/2,
                text="Game Over", fill="white")
                

class Protopong(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        parent.title('Protopong')
        self.board = Board(self.parent, self)
        self.initUI()
        self.pack()

    def initUI(self):

        restart = Button(self, text="Restart", command=self.onClick)
        restart.grid(row=1, column = 2)
        quit = Button(self, text = " Quit  ", command=self.quit)
        quit.grid(row=1, column=4)
        
    def onClick(self):
        self.board.delete(ALL)
        self.board.initGame()

def main():
    root = Tk()
    prot = Protopong(root)
    root.mainloop()
    root.destroy()
if __name__ == '__main__':
    main()
