#Pong Game Paddles Prototype

import sys
from Tkinter import *
import numpy as np
from controller import *

WIDTH = 600
HEIGHT = 600
DELAY =  25   

class Board(Canvas):

    def __init__(self, parent, Frame):
        field = Canvas.__init__(self, width=600, height=300,
                                    background="black", highlightthickness=0)
        
        self.Frame = Frame      # this is the GUI Frame passed back from protopong
        self.parent = parent
        self.initUI()
        self.initGame()
        self.pack()

    def initUI(self):

        #This function defines the score labels.
        label_p1 = Label(self.Frame, text = "  Player 1                               ")
        label_p1.grid(row = 0, column = 0, columnspan=2)

        label_p2 = Label(self.Frame, text = "                               Player 2  ")
        label_p2.grid(row = 0, column = 8, columnspan=2)

        label_score = Label(self.Frame, text = "  Score  ")
        label_score.grid(row = 0, column = 3) 
        
        self.player1_points = IntVar()
        self.label_p1_score_value = Label(self.Frame,textvariable=self.player1_points)
        self.label_p1_score_value.grid(row = 0, column = 2)
        
        self.player2_points = IntVar()
        self.label_p2_score_value = Label(self.Frame,textvariable = self.player2_points)
        self.label_p2_score_value.grid(row = 0, column = 4)        

    def initGame(self):

        #This starts the game
        #It defines the score variable, binds the keyboard controls
        #It calls createobjects to draw the inital images on the screen
        self.score =  [0, 0]          # added to keep score
        self.count = 0                # to count how many times ball hits paddle
        self.focus_get()
        self.createObjects()
        self.bind_all("<Key>", self.onKeyPressed)
        self.after(DELAY, self.onTimer)

    #These are functions to draw images of game objects.
    #They are used in createobjects for the initial drawing
    #Then the createball is reused in the reset after score 
    def createPaddle(self, line, thickness, name):
        image = self.create_rectangle(line.p1[0], line.p1[1], line.p2[0]+thickness, 
        line.p2[1], fill="white", tag=name)
        self.addtag_withtag("paddle", name)
        return image

    def createBorder(self, line):
        image = self.create_line(line.positions, fill="white", tag="border")
        return image

    def createBall(self, circle):
        r = circle.radius
        x, y = circle.position
        image = self.create_oval(x-r, y-r, x+r, y+r, fill = "white", tag = "ball")
        return image

    def createObjects(self):

        #This is where I define the game objects based on classes in controller.
        self.paddle_A = Line([550,100,550,50])
        self.paddle_B = Line([50, 50, 50, 100])
        self.ball = Circle([300,150], [-1,1]) 

        wall_names = ["lower", "upper"] #These are border lines
        line_points = [[0,300,600,300],[600,0,0,0]]
        self.wall_dict = dict(zip(wall_names, map(Line, line_points))) 
        #The above dictionary is not really necessary but may be useful for making several line objects at once.   
       

        #This is where I put the above game objects in the draw functions.
        self.padA_im = self.createPaddle(self.paddle_A, 10, "paddle_A")
        self.padB_im = self.createPaddle(self.paddle_B, -10, "paddle_B")
        self.ball_im = self.createBall(self.ball)

        for k,v in self.wall_dict.items():
            self.createBorder(self.wall_dict[k]) #Uses dictionary of border lines


    def onTimer(self):

        #This function calls other functions every time step
        #CheckCollision sees if there is a hit
        #doMove moves ball and checks score
        self.checkCollision()
        self.doMove()
        self.after(DELAY, self.onTimer)


    def checkCollision(self):

        #This uses methods from the Line and Ball class in the controller module.
        #I had to change the direction of the lines for the walls.
        #THe sign of the unit normal vector depends on how the lines are defined.
        #So not sure if I have the best way of calculating the reflected path.
        #It works for this but lines for obstacles will need improvemnt.
        
        for k,v in self.wall_dict.items():
            if v.hit_check(self.ball.position, self.ball.radius):
                self.ball.new_velocity(v.unormal)

        for item in [self.paddle_A, self.paddle_B]:
            if item.hit_check(self.ball.position, self.ball.radius):
                self.ball.new_velocity(item.unormal)
                self.count += 1
                if self.count == 5:
                    self.count = 0
                    self.ball.velocity += self.ball.velocity

    
    def reset_after_score(self):
        #This function resets game and updates the score labels.

        self.player1_points.set(self.score[0])
        self.player2_points.set(self.score[1])

        self.ball.position = [300, 150]
        self.delete(self.ball_im)
        self.ball_im = self.createBall(self.ball)
        self.count = 0


    def doMove(self):

        if self.coords(self.ball_im)[0] < 0:
            self.score[1] +=1
            self.reset_after_score()
            
        elif self.coords(self.ball_im)[0] > 600:
            self.score[0] += 1
            self.reset_after_score()
        else:
            self.ball.new_position()
            self.coords(self.ball_im,(self.ball.position[0]-5, self.ball.position[1]-5, 
                self.ball.position[0]+5, self.ball.position[1]+5))


    def onKeyPressed(self, e):

        #This function sets keyboard controls.
        key = e.keysym

        if key == "Up" :
            if self.coords(self.padA_im)[3] > 40 :
                self.paddle_A.move_line(-1)
                self.move(self.padA_im, 0, -self.paddle_A.step)

        if key == "Down":
            if self.coords(self.padA_im)[1] <260:
                self.paddle_A.move_line(1)
                self.move(self.padA_im, 0, self.paddle_A.step)
            
        if key == "e":
            if self.coords(self.padB_im)[3] > 40:
                self.paddle_B.move_line(-1)
                self.move(self.padB_im, 0, -self.paddle_B.step)
            
        if key == "s":
            if self.coords(self.padB_im)[1] < 260:
                self.paddle_B.move_line(1)
                self.move(self.padB_im, 0, self.paddle_B.step)
   


class Protopong(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        parent.title('Protopong')
        self.board = Board(self.parent, self)
        self.initUI()
        self.pack()

    def initUI(self):

        restart = Button(self, text="Restart", command=self.onClick)
        restart.grid(row=1, column = 2)
        quit = Button(self, text = " Quit  ", command=self.quit)
        quit.grid(row=1, column=4)

    def onClick(self):
        self.board.delete(ALL)
        self.board.initGame()

def main():
    root = Tk()
    prot = Protopong(root)
    root.mainloop()
    root.destroy()
if __name__ == '__main__':
    main()
